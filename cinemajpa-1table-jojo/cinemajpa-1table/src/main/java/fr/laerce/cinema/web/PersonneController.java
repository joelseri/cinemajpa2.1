package fr.laerce.cinema.web;

import fr.laerce.cinema.dao.PersonneDao;
import fr.laerce.cinema.model.Personne;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

// tout ce qui passe par ce controller passe par /person
@Controller
@RequestMapping(value = "/person")
public class PersonneController {

    @Autowired
    PersonneDao personneDao;


    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("personnes", personneDao.getAll());
        return "person/list";
    }

    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") long id, Model model) {
        model.addAttribute("personne", personneDao.getById(id));
        return "person/detail";
    }

    @GetMapping("/mod/{id}")
    public String mod(@PathVariable("id") long id, Model model) {
        Personne p = personneDao.getById(id);
        System.out.println(p);
        model.addAttribute("personne", p);
        return "person/form";
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id) {
        personneDao.delete(id);
        //on return la chaine string index de façon à ouvrir index.html
        return "person/delete";
    }

    @GetMapping("/add")
    public String add(Model model) {
        Personne personne = new Personne();
        model.addAttribute("personne", personne);
        return "person/form";
    }

    @PostMapping("/add")
    public String submit(@ModelAttribute Personne personne) {
        personneDao.save(personne);
        return "redirect:/person/list";
    }

/*
    @Autowired
    public void setPersonneRepository(PersonneRepository productRepository) {
        this.personneRepository = personneRepository;
    }
    @RequestMapping(path = "/")
    public String index() {
        return "index";
    }
    @RequestMapping(path = "/personne/add", method = RequestMethod.GET)
    public String createProduct(Model model) {
        model.addAttribute("personnes", new Personne());
        return "edit";
    }
    @RequestMapping(path = "personne", method = RequestMethod.POST)
    public String saveProduct(Personne personne) {
        productRepository.save(personne);
        return "redirect:/";
    }
    @RequestMapping(path = "/personnes", method = RequestMethod.GET)
    public String getAllPersonne(Model model) {
        model.addAttribute("personnes", personneRepository.findAll());
        return "personnes";
    }
    @RequestMapping(path = "/personnes/edit/{id}", method = RequestMethod.GET)
    public String editPersonne(Model model, @PathVariable(value = "id") int id) {
        model.addAttribute("personne", personneDao.getById(id));
        return "edit";
    }
    @RequestMapping(path = "/personnes/delete/{id}", method = RequestMethod.GET)
    public String deletePersonnes(@PathVariable(name = "id") int id) {
        personneRepository.delete(id);
        return "redirect:/personnes";
    }*/
}
